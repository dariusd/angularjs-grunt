'use strict';

var protocol = 'http', 
    serverIP = '127.0.0.1', // or 'localhost'
    portNumber = '5000', 
    serviceURL = '/';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'views/html/view2.html',
    controller: 'View2Ctrl'
  });
  delete $httpProvider.defaults.headers.common['X-Requested-With']; // for allowing CORS requests
}])

.controller('View2Ctrl', ['$scope', '$http', function($scope, $http) {
    $scope.myData = {};
    
    var rspPromise = $http.get(protocol + '://' + serverIP + ":" + portNumber + serviceURL);
    rspPromise.success(function(data, status, header, config) {
        $scope.myData = data;
    });
    rspPromise.error(function(data, status, header, config) {
        console.log("AJAX failed!");
    });
}]);